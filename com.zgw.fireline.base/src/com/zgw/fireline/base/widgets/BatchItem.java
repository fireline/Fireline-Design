package com.zgw.fireline.base.widgets;


/**
 * 批处理项
 * */
public class BatchItem {
	protected String name;
	protected int type;
	protected String command;// 执行命令 脚本
	protected Object[] params = new Object[0];
	protected final Batch parent;

	public BatchItem(Batch parent) {
		this.parent = parent;
		this.parent.addItem(this);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Object[] getParams() {
		return params;
	}

	public void setParam(int index, Object value) {
		if (params.length <= index) {
			Object[] newParams = new Object[index + 1];
			System.arraycopy(params, 0, newParams, 0, params.length);
			params = newParams;
		}
		params[index] = value;
	}

}

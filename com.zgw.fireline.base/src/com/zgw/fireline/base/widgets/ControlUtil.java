package com.zgw.fireline.base.widgets;

import java.math.BigDecimal;
import java.text.NumberFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import com.zgw.fireline.base.common.Assert;

public class ControlUtil {

	/**
	 * 
	 * 限定指定的 文本框只能输入 Double 类型的数字
	 * 
	 * @param text
	 *            需要添加规则的控件
	 * @param tip
	 *            输入错误是否提示
	 * @param isNull
	 *            是否可以为空
	 * */
	public static void setDoubleRule(final Text text, final boolean tip,
			final boolean isNull) {
		Listener doubleListener = new Listener() {
			public void handleEvent(Event event) {
				Text t = (Text) event.widget;
				StringBuffer sb = new StringBuffer(t.getText());
				sb.replace(event.start, event.end, event.text);
				String s = sb.toString();
				if (s.equals("")) {
					if (!isNull) {
						t.setText("0");
						t.selectAll();
						event.doit = false;
					}
					return;
				}
				if (s.equals("-")) {
					if (event.character == SWT.BS) {
						t.setText("0");
						t.selectAll();
					} else {
						t.setText("-0");
						t.setSelection(1, 2);
					}
					event.doit = false;
					return;
				}
				try {
					BigDecimal big = new BigDecimal(s);
				} catch (NumberFormatException e) {
					event.doit = false;
				}
			}
		};
		text.addListener(SWT.Verify, doubleListener);
	}

	/**
	 * 设置货币输入规则
	 * */
	public static void setCurrencyRule(final Text text, final boolean tip,
			final boolean isNull) {
		setDoubleRule(text, tip, isNull);
		text.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
				if (text.getText().equals("") || text.isFocusControl()
						|| !text.getEnabled()) {
					return;
				}
				Color old = e.gc.getForeground();
				e.gc.setForeground(text.getBackground());
				e.gc.fillRectangle(0, 0, e.width, e.height);
				e.gc.setForeground(old);
				String formatText = NumberFormat.getCurrencyInstance().format(
						new BigDecimal(text.getText()));
				e.gc.drawText(formatText, 0, 0);
			}
		});

	}

	/**
	 * 按下回车键焦点移动至下一个控件 。 </p> 移动的顺序 与父面版的<b>control.getParent().getTabList()</b>
	 * 有关
	 * */
	public static void setEnterToFocusMove(Control control) {
		control.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.KEYPAD_CR || e.keyCode == SWT.CR) {
					Event tabEvent = new Event();
					tabEvent.keyCode = SWT.TAB;
					tabEvent.type = SWT.KeyDown;
					e.display.post(tabEvent);// 触发 tab 事件 以达到焦点移动至下一个控件
				}
			}
		});
	}

	public static void centerShell(Shell shell) {
		if (shell == null || shell.isDisposed())
			return;
		Rectangle displayBounds = shell.getDisplay().getPrimaryMonitor()
				.getBounds();
		Rectangle shellBounds = shell.getBounds();
		int x = displayBounds.x + (displayBounds.width - shellBounds.width) >> 1;
		int y = displayBounds.y + (displayBounds.height - shellBounds.height) >> 1;
		shell.setLocation(x, y);
	}

	public static void setTreeExpandedAll(Tree tree, boolean expanded) {
		Assert.isNotNull(tree);
		Assert.isTrue(!tree.isDisposed());
		for (TreeItem item : tree.getItems()) {
			if (item != null && !item.isDisposed())
				setTreeItemExpandedAll(item, expanded);
		}
	}

	public static void setTreeItemExpandedAll(TreeItem item, boolean expanded) {
		Assert.isTrue(!item.isDisposed());
		if (item.getItemCount() > 0) {
			item.setExpanded(true);
			for (TreeItem i : item.getItems()) {
				if (i != null && !i.isDisposed())
					setTreeItemExpandedAll(i, expanded);
			}
		}
	}
}

package com.zgw.fireline.base.controls;

import java.util.EventObject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.zgw.fireline.base.dataset.Dataset;
import com.zgw.fireline.base.dataset.DatasetListener;

public class DataTable extends Table {
	private final Dataset dataset;
	protected int rowHeight = 0;

	public DataTable(Composite parent, int style, Dataset set) {
		super(parent, style);
		this.dataset = set;
		if (dataset != null) {
			addListener(SWT.Selection, new Listener() {
				public void handleEvent(Event event) {
					Integer index = (Integer) getSelection()[0]
							.getData("DATASET_INDEX");
					if (index != null)
						dataset.setSelected(index);
				}
			});

			final DatasetListener listener = new DatasetListener() {
				public void rowSetChanged(EventObject event) {
					refresh();
				}

				public void rowChanged(EventObject event) {

				}

				public void cursorMoved(EventObject event) {
					if (dataset.getSelectedIndex() == -1) {
						deselectAll();
					} else
						setSelection(dataset.getSelectedIndex()); // DATASET_INDEX
																	// 来查找选择项
				}
			};
			dataset.addDatasetListener(listener);
			addDisposeListener(new DisposeListener() {
				public void widgetDisposed(DisposeEvent e) {
					if (dataset != null)
						dataset.removeDatasetListener(listener);
				}
			});
		}
		addListener(SWT.MeasureItem, new Listener() {
			public void handleEvent(Event event) {
				if (rowHeight > 0)
					event.height = rowHeight;
			}
		});
	}

	@Override
	public void showSelection() {
		super.showSelection();
		Integer index;
		if (dataset != null
				&& (index = (Integer) getSelection()[0]
						.getData("DATASET_INDEX")) != null) {
			dataset.setSelected(index);
		}
	}

	public void refresh() {
		removeAll();
		TableItem item;
		DataTableColumn dcolumn;
		for (int i = 0; i < dataset.getCount(); i++) {
			item = new TableItem(this, SWT.NONE);
			for (TableColumn c : getColumns()) {
				if (c instanceof DataTableColumn) {
					dcolumn = (DataTableColumn) c;
					String column = dcolumn.getColumnName();
					DataLabelProvider label = ((DataTableColumn) c)
							.getLabelProvider();
					String value = null;
					if (label != null) {
						value = label.getText(i, column, dataset);
					} else if (dcolumn.getShowText() != null) {
						value = dataset.getStringForExpr(i,
								dcolumn.getShowText());
					} else {
						value = dataset.getValueForIndex(i, column,
								String.class);
					}
					item.setText(indexOf(c), value == null ? "" : value);
					item.setData("DATASET_INDEX", i);
				}
			}
			if (i == dataset.getSelectedIndex()) {
				setSelection(item);
			}
		}
	}

	public Object getValue(TableItem item, String columnName) {
		return dataset.getValueForIndex(
				(Integer) item.getData("DATASET_INDEX"), columnName);
	}

	@Override
	protected void checkSubclass() {

	}

	public int getRowHeight() {
		return rowHeight;
	}

	public void setRowHeight(int rowHeight) {
		this.rowHeight = rowHeight;
	}
}

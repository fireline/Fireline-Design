package com.zgw.fireline.base.test;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.zgw.fireline.base.controls.DataCombo;
import com.zgw.fireline.base.controls.DataTable;
import com.zgw.fireline.base.controls.DataTableColumn;
import com.zgw.fireline.base.controls.DataText;
import com.zgw.fireline.base.dataset.Dataset;

public class TestDlgSqlserver extends Dialog {

	protected Object result;
	protected Shell shell;
	private TestSqlserverProvide testSqlserverProvide = new TestSqlserverProvide();
	/**
	 * @wbp.nonvisual location=117,482
	 */
	private final Dataset dataset = new Dataset(
			"select * from dbo.Telement t where (? =0 or t.fcode=?) and (? =0 or t.fname like '%'+?+'%')\r\nand (?=0 or t.ftype=?)",
			testSqlserverProvide);
	private Text text;
	private Text text_1;
	/**
	 * @wbp.nonvisual location=17,482
	 */
	private final Dataset dataset_1 = new Dataset("select * from TelementType",
			testSqlserverProvide);
	private DataCombo combo;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public TestDlgSqlserver(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setSize(615, 416);
		shell.setText(getText());
		shell.setLayout(new FormLayout());
		DataTable table = new DataTable(shell, SWT.BORDER | SWT.FULL_SELECTION
				| SWT.HIDE_SELECTION, dataset);
		FormData fd_table = new FormData();
		fd_table.bottom = new FormAttachment(0, 236);
		fd_table.right = new FormAttachment(0, 494);
		fd_table.top = new FormAttachment(0, 10);
		fd_table.left = new FormAttachment(0, 10);
		table.setLayoutData(fd_table);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		DataTableColumn dtblclmnId = new DataTableColumn(table, SWT.NONE, "fid");
		dtblclmnId.setText("ID");
		dtblclmnId.setWidth(55);
		DataTableColumn column1 = new DataTableColumn(table, SWT.NONE, "fcode");
		column1.setWidth(100);
		column1.setText("编号");

		DataTableColumn dataTableColumn = new DataTableColumn(table, SWT.NONE,
				"fname");
		dataTableColumn.setText("名称");
		dataTableColumn.setWidth(100);

		DataTableColumn dataTableColumn_1 = new DataTableColumn(table,
				SWT.NONE, "ftype");
		dataTableColumn_1.setWidth(90);
		dataTableColumn_1.setText("类别");

		DataTableColumn dataTableColumn_2 = new DataTableColumn(table,
				SWT.NONE, "fremark");
		dataTableColumn_2.setWidth(97);
		dataTableColumn_2.setText("备注");

		Button btnNewButton = new Button(shell, SWT.NONE);
		FormData fd_btnNewButton = new FormData();
		fd_btnNewButton.bottom = new FormAttachment(100, -10);
		fd_btnNewButton.left = new FormAttachment(table, 0, SWT.LEFT);
		fd_btnNewButton.right = new FormAttachment(0, 84);
		btnNewButton.setLayoutData(fd_btnNewButton);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doRefresh();
			}
		});
		btnNewButton.setText("刷新");

		Composite composite = new Composite(shell, SWT.BORDER);
		composite.setBackground(SWTResourceManager
				.getColor(SWT.COLOR_TITLE_INACTIVE_FOREGROUND));
		fd_btnNewButton.top = new FormAttachment(0, 354);
		FormData fd_composite = new FormData();
		fd_composite.bottom = new FormAttachment(btnNewButton, -6);
		fd_composite.top = new FormAttachment(table, 6);
		fd_composite.left = new FormAttachment(0, 10);
		fd_composite.right = new FormAttachment(0, 494);
		composite.setLayoutData(fd_composite);
		composite.setLayout(new GridLayout(5, false));

		Label lblNewLabel = new Label(composite, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel.setText("编号");

		text = new Text(composite, SWT.BORDER);
		text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

		Label lblNewLabel_1 = new Label(composite, SWT.NONE);
		lblNewLabel_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_1.setText("名称");

		text_1 = new Text(composite, SWT.BORDER);
		text_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		Combo combo_2 = new Combo(composite, SWT.NONE);
		combo_2.setFont(SWTResourceManager.getFont("宋体", 15, SWT.NORMAL));
		combo_2.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

		Label lblNewLabel_2 = new Label(composite, SWT.NONE);
		lblNewLabel_2.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_2.setText("类别");

		combo = new DataCombo(composite, SWT.NONE, dataset_1);
		combo.setShowText("${fname} - ${fcode}");
		combo.setFont(SWTResourceManager.getFont("宋体", 11, SWT.NORMAL));
		GridData gd_combo = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_combo.heightHint = 45;
		combo.setLayoutData(gd_combo);

		new Label(composite, SWT.NONE);

		DataText dataText = new DataText(composite, SWT.NONE, (Dataset) null,
				(String) null);
		dataText.setFont(SWTResourceManager.getFont("宋体", 11, SWT.NORMAL));
		dataText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false,
				1, 1));

		CCombo combo_1 = new CCombo(composite, SWT.BORDER);
		combo_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1,
				1));
		dataset_1.update();
	}

	protected void doRefresh() {
		if (text.getText().equals("")) {
			dataset.setParam(0, 0);
		} else {
			dataset.setParam(0, 1);
		}
		dataset.setParam(1, text.getText());
		if (text_1.getText().equals("")) {
			dataset.setParam(2, 0);
		} else {
			dataset.setParam(2, 1);
		}
		dataset.setParam(3, text_1.getText());

		if (combo.getText().equals("")) {
			dataset.setParam(4, 0);
		} else {
			dataset.setParam(4, 1);
		}
		dataset.setParam(5, dataset_1.getValue("fid"));
		// null
		dataset.update();
	}

	public static void main(String[] args) {
		TestDlgSqlserver dlg = new TestDlgSqlserver(new Shell(), SWT.NONE);
		dlg.open();
	}
}

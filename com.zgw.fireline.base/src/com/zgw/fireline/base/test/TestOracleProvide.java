package com.zgw.fireline.base.test;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.rowset.CachedRowSet;

import com.sun.rowset.CachedRowSetImpl;
import com.zgw.fireline.base.DatasetDefine;
import com.zgw.fireline.base.IDataBaseProvide;

public class TestOracleProvide implements IDataBaseProvide {

	public CachedRowSet queryForDefine(String defineKey, Object[] params) {
		DatasetDefine define = getDataset(defineKey);
		if (define == null) {
			throw new RuntimeException("未找到数据集: '" + defineKey + "'");
		}
		String sql = define.getCommand();
		return queryForSql(sql, params);
	}

	public CachedRowSet queryForSql(String sql, Object[] params) {
		try {
			Connection con = getConnection();
			PreparedStatement p = con.prepareStatement(sql);
			int i = 1;
			for (Object obj : params) {
				if (obj instanceof Date) {
					p.setDate(i++, new java.sql.Date(((Date) obj).getTime()));
				} else {
					p.setObject(i++, obj);
				}
			}
			System.out.println(sql);
			ResultSet r = p.executeQuery();
			CachedRowSetImpl set = new CachedRowSetImpl();
			set.populate(r);
			p.close();
			con.close();
			return set;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	static Connection con;

	public Connection getConnection() throws ClassNotFoundException,
			SQLException {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con = DriverManager.getConnection(
				"jdbc:oracle:thin:@192.168.11.31:1522:ztsys2", "zy", "122333");
		// if (con == null || con.isClosed()) {
		// con = DriverManager
		// .getConnection(
		// "jdbc:oracle:thin:@(description=(address_list= (address=(host=192.168.10.1) (protocol=tcp)(port=1521))(address=(host=192.168.10.2)(protocol=tcp) (port=1521)) (load_balance=yes)(failover=yes))(connect_data=(service_name= ztsys)))",
		// "hisbase", "ztkjbafy");

		// }
		return con;
	}

	public int getParameterCount(String sql) {
		try {
			Connection conn = getConnection();
			PreparedStatement meta = conn.prepareStatement(sql);
			ParameterMetaData param = meta.getParameterMetaData();
			meta.close();
			conn.close();
			return param.getParameterCount();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public int[] execute(String[] command, Map<Integer, Object[]> params) {
		Connection conn = null;
		int[] result = new int[command.length];
		try {
			conn = getConnection();
			for (int i = 0; i < command.length; i++) {
				PreparedStatement statment = conn.prepareStatement(command[i]);
				Object[] p = params.get(i);
				for (int k = 0; k < p.length; k++) {
					statment.setObject(k + 1, p[k]);
				}
				result[i] = statment.execute() ? 1 : 0;
				statment.close();
			}
			conn.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return result;
	}

	public List<DatasetDefine> getAllDataset() {
		try {
			List<DatasetDefine> list = (List<DatasetDefine>) ObjectToXMLUtil
					.objectXmlDecoder(getDefineBeanXml());
			list = list == null ? new ArrayList<DatasetDefine>() : list;
			return list;
		} catch (Exception e) {
			throw new RuntimeException("获取数据集定义失败", e);
		}
	}

	public boolean saveDataset(DatasetDefine dataset) {
		try {
			List<DatasetDefine> list = (List<DatasetDefine>) ObjectToXMLUtil
					.objectXmlDecoder(getDefineBeanXml());
			list = list == null ? new ArrayList<DatasetDefine>() : list;
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getId().equals(dataset.getId())) {
					list.remove(i);
					break;
				}
			}
			list.add(dataset);
			ObjectToXMLUtil.objectXmlEncoder(list, getDefineBeanXml());
		} catch (Exception e) {
			throw new RuntimeException("保存数据集定义失败", e);
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public boolean removeDataset(String defineKey) {
		try {
			List<DatasetDefine> list = (List<DatasetDefine>) ObjectToXMLUtil
					.objectXmlDecoder(getDefineBeanXml());
			list = list == null ? new ArrayList<DatasetDefine>() : list;
			DatasetDefine removeDefine = null;
			for (DatasetDefine d : list) {
				if (d.getId().equals(defineKey)) {
					removeDefine = d;
				}
			}
			if (removeDefine != null) {
				list.remove(removeDefine);
				ObjectToXMLUtil.objectXmlEncoder(list, getDefineBeanXml());
			} else {
				throw new RuntimeException("找不到指定的数据集:'" + defineKey + "'");
			}
		} catch (Exception e) {
			throw new RuntimeException("保存数据集定义失败", e);
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	public DatasetDefine getDataset(String defineKey) {
		try {
			List<DatasetDefine> list;
			list = (List<DatasetDefine>) ObjectToXMLUtil
					.objectXmlDecoder(getDefineBeanXml());
			list = list == null ? new ArrayList<DatasetDefine>() : list;
			for (DatasetDefine d : list) {
				if (d.getId().equals(defineKey)) {
					return d;
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("查找数据集定义失败:'" + defineKey + "'", e);
		}

		return null;
	}

	private String getDefineBeanXml() throws IOException {
		String path = "F:/wbWorkspace/com.zgw.fireline.base/src/com/zgw/fireline/base/test/DatasetDefineBean.xml";
		File file = new File(path);
		if (!file.exists()) {
			file.createNewFile();
		}
		return path;
	}
}

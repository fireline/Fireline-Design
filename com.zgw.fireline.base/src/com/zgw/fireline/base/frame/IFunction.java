package com.zgw.fireline.base.frame;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * 系统功能接口
 * */
public interface IFunction {
	public static final int Add = 1;
	public static final int Edit = 2;

	/**
	 * 创建程序界面
	 * */
	public Control createControl(Composite parent);
}

package com.zgw.fireline.base.dataset;

/** 
 * @author DT E-mail: dengtao@chinacpby.com
 * @version 1.0 创建时间：Jun 28, 20099:23:04 PM
 */
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** */
/**
 * 通用的Object包装类(类型问题，依然是个瓶颈，如果有好的解决方案请pm我)
 * 
 * 功能：查询对象类型或对象集合时的通用包装类
 * 
 * @author zdw
 * 
 */
@SuppressWarnings("unchecked")
public class ObjectMapper {
	private Class clazz;
	private List<String> xmlColumnList = null;

	public ObjectMapper(Class clazz) {
		this.clazz = clazz;
	}

	public ObjectMapper(Class clazz, String[] xmlString) {
		this.clazz = clazz;
		if (xmlString != null && xmlString.length > 0) {
			xmlColumnList = new ArrayList<String>();
			for (String str : xmlString) {
				xmlColumnList.add(str);
			}
		}
	}

	/** */
	/**
	 * 重写mapRow方法
	 */
	public Object mapRow(ResultSet rs) throws SQLException {
		try {
			Map<String, String> map = new HashMap<String, String>();
			ResultSetMetaData md = rs.getMetaData();
			for (int i = 0; i < md.getColumnCount(); i++) {
				String columnName = md.getColumnName(i + 1);
				if (columnName == null)
					columnName = "";
				map.put(columnName.toLowerCase(), columnName);
			}

			Object obj = clazz.newInstance();
			Field fields[] = obj.getClass().getDeclaredFields();

			for (int i = 0; i < fields.length; i++) {
				Field field = fields[i];
				if (field.getName().equals("serialVersionUID")
						|| map.get(field.getName().toLowerCase()) == null)
					continue;
				// 暴力访问
				field.setAccessible(true);
				this.typeMapper(field, obj, rs);
				// 恢复默认
				field.setAccessible(false);
			}
			return obj;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/** */
	/**
	 * 数据类型包装器
	 * 
	 * @param field
	 *            目标属性
	 * @param obj
	 *            目标对象
	 * @param rs
	 *            结果集
	 * @throws Exception
	 */
	private void typeMapper(Field field, Object obj, ResultSet rs)
			throws Exception {
		String type = field.getType().getName();
		if (type.equals("java.lang.String")) {
			field.set(obj, rs.getString(field.getName()));
		} else if (type.equals("int")) {
			field.set(obj, rs.getInt(field.getName()));
		} else if (type.equals("java.lang.Integer")) {
			if (rs.getString(field.getName()) == null
					|| "".equals(rs.getString(field.getName()))) {
				field.set(obj, null);
			} else {
				field.set(obj, rs.getInt(field.getName()));
			}
		} else if (type.equals("long")) {
			field.set(obj, rs.getLong(field.getName()));
		} else if (type.equals("java.lang.Long")) {
			if (rs.getString(field.getName()) == null
					|| "".equals(rs.getString(field.getName()))) {
				field.set(obj, null);
			} else {
				field.set(obj, rs.getLong(field.getName()));
			}
		} else if (type.equals("boolean") || type.equals("java.lang.Boolean")) {
			field.set(obj, rs.getBoolean(field.getName()));
		} else if (type.equals("java.util.Date")) {
			// field.set(obj, rs.getDate(field.getName()));
			field.set(obj, rs.getTimestamp(field.getName()));
		} else if (type.equals("double") || type.equals("java.lang.Double")) { // Double型包装
			field.set(obj, rs.getDouble(field.getName()));
		} else if (type.equals("float") || type.equals("java.lang.Float")) {
			field.set(obj, rs.getFloat(field.getName()));
		}else if(type.equals("java.math.BigDecimal")){
			field.set(obj, rs.getBigDecimal(field.getName()));
		}
	}
}
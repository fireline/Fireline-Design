package com.zgw.fireline.base;

import java.util.List;
import java.util.Map;

import javax.sql.rowset.CachedRowSet;

/**
 * 与数据层交互接口，用于解析执行sql 语句，或调用底层服务方法。
 * */
public interface IDataBaseProvide {

	public List<DatasetDefine> getAllDataset();

	public boolean saveDataset(DatasetDefine dataset);

	public boolean removeDataset(String defineKey);

	public DatasetDefine getDataset(String defineKey);

	/**
	 * 查询数据库，返回CachedRowSet
	 * */
	public CachedRowSet queryForDefine(String defineKey, Object[] param);

	/**
	 * 查询数据库，返回CachedRowSet
	 * */
	public CachedRowSet queryForSql(String sql, Object[] params);

	/**
	 * 返回sql 参数个数
	 * */
	public int getParameterCount(String sql);

	/**
	 * 按顺序执行命令集
	 * */
	public int[] execute(String[] command, Map<Integer, Object[]> params);

}

package com.zgw.fireline.design;

import java.io.File;
import java.net.URL;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import org.eclipse.core.internal.resources.Resource;
import org.eclipse.core.internal.resources.Workspace;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.internal.ui.javaeditor.JavaEditor;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.WorkbenchPlugin;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.wb.internal.core.editor.multi.DesignerEditor;
import org.eclipse.wb.internal.core.utils.IOUtils2;

import com.zgw.fireline.design.common.FileUtil;
import com.zgw.fireline.design.common.ProjectUtil;
import com.zgw.fireline.design.service.IDesignCommonService;

@SuppressWarnings("restriction")
public class DesignCommonServiceImpl extends UnicastRemoteObject implements
		IDesignCommonService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1929788736657919419L;
	ProjectUtil putil = new ProjectUtil();

	public DesignCommonServiceImpl() throws RemoteException {
		super();
	}

	public void openWidgetEdit(File outPut, String projectName, URL src,
			URL[] classPaths, String widgetName) throws RemoteException {
		final Workspace WORKSPACE = (Workspace) ResourcesPlugin.getWorkspace();
		try {
			Resource project = WORKSPACE.newResource(new Path(projectName),
					IResource.PROJECT);
			if (!project.exists()) {
				putil.createJavaProject(projectName);
			}
			putil.setOutPut(projectName, outPut);
			putil.iniClassPath(classPaths, projectName);
			String pathTxt = projectName + "/src/"
					+ widgetName.replace(".", "/") + ".java";
			IPath path = new Path(pathTxt);
			final IFile file = (IFile) WORKSPACE.newResource(path,
					IResource.FILE);
			URL fileUrl = new URL(src, widgetName.replace(".", "/") + ".java");
			if (file.exists()) {
				file.setContents(fileUrl.openStream(), true, false, null);
			} else {
				IOUtils2.ensureFolderExists(file);
				file.create(fileUrl.openStream(), true, null);
			}

			Display.getDefault().syncExec(new Runnable() {
				public void run() {
					IWorkbenchWindow window = PlatformUI.getWorkbench()
							.getWorkbenchWindows()[0];
					IEditorInput input = new FileEditorInput(file);
					try {
						// JavaEditor ID:
						// org.eclipse.jdt.ui.CompilationUnitEditor
						JavaEditor part = (JavaEditor) window.getActivePage()
								.openEditor(input,
										"org.eclipse.wb.core.guiEditor");
						DesignerEditor edit = (DesignerEditor) part;
						// edit.getMultiMode().showDesign();
					} catch (PartInitException e) {
						e.printStackTrace();
						WorkbenchPlugin.log(file.toString(), e);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			WorkbenchPlugin.log(projectName, e);
		}
	}

	public boolean cleanBuild(String projectName, File outPut)
			throws RemoteException {
		final Workspace WORKSPACE = (Workspace) ResourcesPlugin.getWorkspace();
		IProject project = (IProject) WORKSPACE.newResource(new Path(
				projectName), Resource.PROJECT);
		try {
			if (project.exists()) {
				putil.setOutPut(projectName, outPut);
				project.build(IncrementalProjectBuilder.CLEAN_BUILD, null);
				return false;
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean delete(String projectName, String path)
			throws RemoteException {
		final Workspace WORKSPACE = (Workspace) ResourcesPlugin.getWorkspace();
		IFolder folder = (IFolder) WORKSPACE.newResource(new Path(projectName
				+ path), Resource.FOLDER);
		if (folder.exists()) {
			File root = new File(Platform.getLocation().toString()
					+ folder.getFullPath().toString());
			boolean result = FileUtil.clearChildren(root);
			WORKSPACE.getRefreshManager().refresh(folder);
			return result;
		}
		return false;
	}

	public String getLocation(String projectName, String path)
			throws RemoteException {
		final Workspace WORKSPACE = (Workspace) ResourcesPlugin.getWorkspace();
		IFolder folder = (IFolder) WORKSPACE.newResource(new Path(projectName
				+ path), Resource.FOLDER);
		if (folder.exists()) {
			return Platform.getLocation().toString()
					+ folder.getFullPath().toString();
		}
		return null;
	}

}

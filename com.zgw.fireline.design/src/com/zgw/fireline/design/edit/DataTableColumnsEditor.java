package com.zgw.fireline.design.edit;

import org.eclipse.swt.widgets.Display;
import org.eclipse.wb.internal.core.model.property.JavaProperty;
import org.eclipse.wb.internal.core.model.property.Property;
import org.eclipse.wb.internal.core.model.property.editor.TextDialogPropertyEditor;

import com.zgw.fireline.design.Model.DataTableInfo;

/**
 * 
 * 表格列编辑
 * */
public class DataTableColumnsEditor extends TextDialogPropertyEditor {

	public static final DataTableColumnsEditor INSTANCE = new DataTableColumnsEditor();

	@Override
	protected void openDialog(Property property) throws Exception {
		if (property instanceof JavaProperty) {
			DataTableInfo table = (DataTableInfo) ((JavaProperty) property)
					.getJavaInfo();
			DataTableColumnsDialog dlg = new DataTableColumnsDialog(Display
					.getDefault().getActiveShell(), table);
			table.startEdit();
			dlg.open();
			table.endEdit();
		}
	}

	@Override
	protected String getText(Property property) throws Exception {
		return "";
	}

}

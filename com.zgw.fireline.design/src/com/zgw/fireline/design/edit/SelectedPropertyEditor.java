package com.zgw.fireline.design.edit;

import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.wb.internal.core.model.property.GenericPropertyImpl;
import org.eclipse.wb.internal.core.model.property.Property;
import org.eclipse.wb.internal.core.model.property.editor.AbstractTextPropertyEditor;
import org.eclipse.wb.internal.core.model.property.editor.presentation.ButtonPropertyEditorPresentation;
import org.eclipse.wb.internal.core.model.property.editor.presentation.PropertyEditorPresentation;
import org.eclipse.wb.internal.core.model.property.table.PropertyTable;
import org.eclipse.wb.internal.core.utils.ast.AstEditor;
import org.eclipse.wb.internal.core.utils.ast.AstNodeUtils;
import org.eclipse.wb.internal.core.utils.ast.StatementTarget;
import org.eclipse.wb.internal.core.utils.execution.ExecutionUtils;
import org.eclipse.wb.internal.core.utils.execution.RunnableEx;

import com.zgw.fireline.design.Model.DatasetCodeUtil;
import com.zgw.fireline.design.Model.DatasetInfo;

/**
 * 选中项绑定属性
 * */
public class SelectedPropertyEditor extends AbstractTextPropertyEditor {

	public SelectedPropertyEditor() {

	}

	// //////////////////////////////////////////////////////////////////////////
	//
	// Presentation
	//
	// //////////////////////////////////////////////////////////////////////////
	private final PropertyEditorPresentation m_presentation = new ButtonPropertyEditorPresentation() {
		@Override
		protected void onClick(final PropertyTable propertyTable,
				Property property) throws Exception {
			final GenericPropertyImpl p = (GenericPropertyImpl) property;
			ExecutionUtils.run(p.getJavaInfo(), new RunnableEx() {
				public void run() throws Exception {
					openDialog(propertyTable, p);
				}
			});

		}
	};

	@Override
	public final PropertyEditorPresentation getPresentation() {
		return m_presentation;
	}

	@Override
	public boolean activate(PropertyTable propertyTable, Property property,
			Point location) throws Exception {
		return false;
	}

	// 打开Dataset 资源管理窗口
	protected void openDialog(final PropertyTable table,
			GenericPropertyImpl property) throws Exception {
		SelectedKeyDialog dlg = new SelectedKeyDialog(table.getShell(),
				SWT.NONE, (DatasetInfo) property.getJavaInfo());
		if (dlg.open() == SWT.OK) {
			Statement statment = AstNodeUtils.getEnclosingStatement(property
					.getExpression());
			AstEditor editor = property.getJavaInfo().getRootJava().getEditor();
			if (statment != null) {
				editor.removeStatement(statment);
			}
			property.setExpression(dlg.source, Property.UNKNOWN_VALUE);
			// 检测并添加 setValues() 方法
			MethodDeclaration setValuesMethod = DatasetCodeUtil
					.buildSetValuesContext(property.getJavaInfo().getRootJava());
			StatementTarget target = new StatementTarget(setValuesMethod, false);
			// 将属性表达示移动至setValues() 方法
			editor.moveStatement(AstNodeUtils.getEnclosingStatement(property
					.getExpression()), target);
		}

	}

	@Override
	protected String getText(Property property) throws Exception {
		return null;
	}

	@Override
	protected String getEditorText(Property property) throws Exception {
		return null;
	}

	@Override
	protected boolean setEditorText(Property property, String text)
			throws Exception {
		return false;
	}

}

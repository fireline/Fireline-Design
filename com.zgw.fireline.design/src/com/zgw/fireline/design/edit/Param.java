package com.zgw.fireline.design.edit;

import java.util.Date;

import org.eclipse.wb.core.model.JavaInfo;
import org.eclipse.wb.internal.core.model.property.converter.DoubleConverter;
import org.eclipse.wb.internal.core.model.property.converter.StringConverter;
import org.eclipse.wb.internal.core.model.variable.VariableSupport;

import com.ibm.icu.text.SimpleDateFormat;

/**
 * 参数值信息
 * */
public class Param {
	public JavaInfo target;
	public String signature;
	public Object value;
	public Class<?> type; // 参数类别
	public String format; // 数据格式

	@Override
	public String toString() {
		String result = "";
		if (value instanceof String) {
			result = (String) value;
		} else if (value instanceof Double) {
			result = String.valueOf(value);
		} else if (value instanceof Date) {
			Date d = (Date) value;
			SimpleDateFormat df = new SimpleDateFormat(format != null ? format
					: "yyyy-MM-dd HH:mm:ss");
			result = df.format(d);
		} else if (target != null && target.isRoot()) {
			result = "this." + signature;
		} else if (target != null) {
			VariableSupport var = target.getVariableSupport();
			result = var.getName() + "." + signature;
		}
		return result;
	}

	public String toJavaSource(JavaInfo javaInfo) throws Exception {
		String result = null;
		if (value instanceof String) {
			result = StringConverter.INSTANCE.toJavaSource(javaInfo, value);
		} else if (value instanceof Double) {
			result = DoubleConverter.INSTANCE.toJavaSource(javaInfo, value);
		} else if (value instanceof Date) {
			Date d = (Date) value;
			SimpleDateFormat df = new SimpleDateFormat(format != null ? format
					: "yyyy-MM-dd HH:mm:ss");
			result = "new Date(" + d.getTime() + "L /*" + df.format(d) + "*/)";
		} else if (target != null && target.isRoot()) {
			result = "this." + signature;
		} else if (target != null) {
			VariableSupport var = target.getVariableSupport();
			if (var.canConvertLocalToField()) {
				var.convertLocalToField();
			}
			result = var.getName() + "." + signature;
		}
		return result;
	}
}
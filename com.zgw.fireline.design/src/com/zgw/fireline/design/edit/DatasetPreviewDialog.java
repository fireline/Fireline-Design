package com.zgw.fireline.design.edit;

import java.io.Reader;
import java.nio.CharBuffer;
import java.sql.Clob;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.rowset.CachedRowSet;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ControlEditor;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

/**
 * 数据集预览窗口
 * */
public class DatasetPreviewDialog extends Dialog {
	private Table table;
	private CachedRowSet set;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public DatasetPreviewDialog(Shell parentShell, CachedRowSet set) {
		super(parentShell);
		setShellStyle(SWT.MAX | SWT.RESIZE);
		this.set = set;
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout gridLayout = (GridLayout) container.getLayout();
		gridLayout.marginHeight = 6;
		gridLayout.marginWidth = 6;
		table = new Table(container, SWT.BORDER | SWT.FULL_SELECTION
				| SWT.HIDE_SELECTION);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		Menu menu = new Menu(table);
		table.setMenu(menu);
		MenuItem itemOpenClob = new MenuItem(menu, SWT.NONE);
		itemOpenClob.setText("查看大文本");
		itemOpenClob.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println(e.x + " " + e.y);
			}
		});
		MenuItem itemOpenImage = new MenuItem(menu, SWT.NONE);
		itemOpenImage.setText("查看图象");
		table.addListener(SWT.MeasureItem, new Listener() {
			public void handleEvent(Event event) {
				event.height = 20;
			}
		});
		try {
			initializeData();
		} catch (SQLException e) {
			e.printStackTrace();
			MessageDialog.openError(getParentShell(), "异常",
					"数据集预览失败：" + e.getMessage());
		}

		return container;
	}

	private void initializeData() throws SQLException {
		if (set == null)
			return;

		// ============构建表格列
		ResultSetMetaData meta = set.getMetaData();
		for (int i = 1; i <= meta.getColumnCount(); i++) {
			int type = meta.getColumnType(i);
			int style = SWT.NONE;
			if (type == Types.NUMERIC || type == Types.INTEGER) // 数字居右
				style = SWT.RIGHT;
			TableColumn column = new TableColumn(table, style);
			column.setMoveable(true);
			column.setResizable(true);
			column.setText(meta.getColumnLabel(i));
			column.setData(meta.getColumnName(i));
			column.setData("type", meta.getColumnType(i));

		}

		// ===========构建表格数据
		TableItem item = null;
		while (set.next()) {
			item = new TableItem(table, SWT.NONE);
			for (int i = 1; i <= table.getColumnCount(); i++) {
				int type = (Integer) table.getColumn(i - 1).getData("type");
				String text;
				if (type == Types.CLOB || type == Types.NCLOB) {
					text = "CLOB";
				} else if (type == Types.BLOB) {
					text = "BLOB";
				} else {
					text = set.getString(i);
				}
				item.setText(i - 1, text == null ? "" : text);
			}
		}
		for (TableColumn c : table.getColumns()) {
			c.pack();
		}
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Button button = createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
		button.setText("关闭");
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(580, 402);
	}

	private String toString(Object obj) throws Exception {
		String result = "";
		if (obj instanceof Clob) {
			Clob clob = (Clob) obj;
			char[] bytes = new char[1024];
			int length = 0;
			Reader read = clob.getCharacterStream();
			StringBuffer sb = new StringBuffer();
			while ((length = read.read(bytes)) > 0) {
				sb.append(bytes, 0, length);
			}
			read.close();
			result = read.toString();
		}
		return result;
	}
}

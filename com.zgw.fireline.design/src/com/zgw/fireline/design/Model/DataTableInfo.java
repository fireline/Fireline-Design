package com.zgw.fireline.design.Model;

import java.util.List;

import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.swt.widgets.Display;
import org.eclipse.wb.core.model.JavaInfo;
import org.eclipse.wb.internal.core.model.JavaInfoUtils;
import org.eclipse.wb.internal.core.model.creation.CreationSupport;
import org.eclipse.wb.internal.core.model.description.ComponentDescription;
import org.eclipse.wb.internal.core.model.property.Property;
import org.eclipse.wb.internal.core.model.property.editor.TextDialogPropertyEditor;
import org.eclipse.wb.internal.core.utils.ast.AstEditor;
import org.eclipse.wb.internal.core.utils.ast.AstNodeUtils;
import org.eclipse.wb.internal.rcp.model.widgets.TableCursorInfo;
import org.eclipse.wb.internal.swt.model.widgets.TableInfo;

import com.zgw.fireline.design.edit.DataTableColumnsDialog;

public final class DataTableInfo extends TableInfo {
	final SimpleProperty columnsProperty; // 数据列设置属性

	public DataTableInfo(AstEditor editor, ComponentDescription description,
			CreationSupport creationSupport) throws Exception {
		super(editor, description, creationSupport);
		columnsProperty = new SimpleProperty(this, "表格列",
				new tableColumnPropertyEditor());
	}

	@Override
	protected List<Property> getPropertyList() throws Exception {
		List<Property> list = super.getPropertyList();
		list.add(columnsProperty);
		return list;
	}

	public DatasetInfo getDataset() {
		TypeDeclaration typeDeclaration = JavaInfoUtils
				.getTypeDeclaration(getRootJava());
		ClassInstanceCreation c = (ClassInstanceCreation) getCreationSupport()
				.getNode();
		SimpleName s = (SimpleName) c.arguments().get(2);
		if (s != null) {
		return	(DatasetInfo) getRootJava().getChildRepresentedBy(s);
			// VariableDeclarationFragment node = AstNodeUtils
			// .getFieldFragmentByName(typeDeclaration,
			// s.getFullyQualifiedName());
			// JavaInfo datasetinfo = DatasetCodeUtil.getJavaInfoByASTNode(
			// getRootJava(), node);
			// return (DatasetInfo) datasetinfo;
		}
		return null;
	}

	/**
	 * @return <code>true</code> if this {@link DataTableInfo} already has
	 *         {@link TableCursorInfo}.
	 */
	public boolean hasTableCursor() {
		return !getChildren(TableCursorInfo.class).isEmpty();
	}

	/**
	 * Adds new {@link TableCursorInfo} to this table.
	 */
	public void command_CREATE(TableCursorInfo tableCursor) throws Exception {
		JavaInfoUtils.add(tableCursor, null, this, null);
	}

	private class tableColumnPropertyEditor extends TextDialogPropertyEditor {
		@Override
		protected void openDialog(Property property) throws Exception {
			DataTableColumnsDialog dlg = new DataTableColumnsDialog(Display
					.getDefault().getActiveShell(), DataTableInfo.this);
			DataTableInfo.this.startEdit();
			dlg.open();
			DataTableInfo.this.endEdit();
		}

		@Override
		protected String getText(Property property) throws Exception {
			return "";
		}

	}
}

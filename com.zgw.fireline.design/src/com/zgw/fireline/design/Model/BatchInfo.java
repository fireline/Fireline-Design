package com.zgw.fireline.design.Model;

import java.util.List;

import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.wb.core.model.JavaInfo;
import org.eclipse.wb.internal.core.model.JavaInfoUtils;
import org.eclipse.wb.internal.core.model.creation.CreationSupport;
import org.eclipse.wb.internal.core.model.description.ComponentDescription;
import org.eclipse.wb.internal.core.model.generation.GenerationUtils;
import org.eclipse.wb.internal.core.model.generation.statement.StatementGenerator;
import org.eclipse.wb.internal.core.model.property.ComplexProperty;
import org.eclipse.wb.internal.core.model.property.GenericProperty;
import org.eclipse.wb.internal.core.model.property.Property;
import org.eclipse.wb.internal.core.model.property.editor.TextDialogPropertyEditor;
import org.eclipse.wb.internal.core.model.variable.LocalUniqueVariableSupport;
import org.eclipse.wb.internal.core.model.variable.VariableSupport;
import org.eclipse.wb.internal.core.utils.ast.AstEditor;
import org.eclipse.wb.internal.core.utils.ast.StatementTarget;
import org.eclipse.wb.internal.core.utils.execution.ExecutionUtils;
import org.eclipse.wb.internal.core.utils.execution.RunnableEx;
import org.eclipse.wb.internal.core.utils.execution.RunnableObjectEx;
import org.eclipse.wb.internal.core.utils.reflect.ReflectionUtils;

import com.zgw.fireline.design.edit.BatchEditDialog;
import com.zgw.fireline.design.edit.Param;

public class BatchInfo extends JavaInfo {
	private SimpleProperty cmdsProperty;

	public BatchInfo(AstEditor editor, ComponentDescription description,
			CreationSupport creationSupport) throws Exception {
		super(editor, description, creationSupport);
		cmdsProperty = new SimpleProperty(this, "批处理编辑",
				new BatchPropertyEditor());
	}

	@Override
	protected List<Property> getPropertyList() throws Exception {
		List<Property> list = super.getPropertyList();
		list.add(cmdsProperty);
		return list;
	}

	/*
	 * 修改批处理属性
	 */
	private void editInfo(final BatchEditDialog dlg) throws Exception {
		TypeDeclaration typeDeclaration = JavaInfoUtils
				.getTypeDeclaration(getRootJava());
		ComplexProperty constructor = (ComplexProperty) getPropertyByTitle("Constructor");
		Property[] cons = constructor.getProperties(); // 构造方法参数
		String source = DatasetCodeUtil.prepareDBInvokeSource(
				dlg.baseProvideClass, typeDeclaration, getEditor());
		((GenericProperty) cons[0]).setExpression(source,
				Property.UNKNOWN_VALUE);
		/*
		 * ===================构建批处理项
		 */
		for (BatchItemInfo c : getChildren(BatchItemInfo.class)) {
			if (!dlg.items.contains(c) && c.canDelete()) {
				c.delete();
			}
		}
		StatementTarget itemTarget = null;
		List<BatchItemInfo> childrenList = getChildren(BatchItemInfo.class);
		if (childrenList.isEmpty()) {// 如果不存在子项代码位置至于第一个控件创建之前
			itemTarget = DatasetCodeUtil.getFistWidgetTarget(getRootJava());
		}
		int index = -1;
		for (BatchItemInfo info : dlg.items) {
			index++;
			if (info.getParent() == null) {
				VariableSupport variableSupport = new LocalUniqueVariableSupport(
						info);
				StatementGenerator statementGenerator = GenerationUtils
						.getStatementGenerator(info);
				if (itemTarget != null) {
					JavaInfoUtils.add(info, variableSupport,
							statementGenerator, null, this, null, itemTarget);
				} else {
					JavaInfoUtils.add(info, variableSupport,
							statementGenerator, null, this, null);
				}
			}
			childrenList = getChildren(BatchItemInfo.class);// 调整 顺序
			if (childrenList.indexOf(info) != index) {
				JavaInfoUtils.move(info, null, this, childrenList.get(index));
				// moveChild(info, childrenList.get(index));
			}
			info.getPropertyByTitle("name").setValue(info.getName());
			info.getPropertyByTitle("command").setValue(info.getCommand());
		}

		/*
		 * ===================更新参数设置
		 */
		String signature = ReflectionUtils.getMethodSignature("setParam",
				new Class[] { int.class, int.class, Object.class });
		removeMethodInvocations(signature); // 移除原来的 参数设置
		MethodDeclaration setParamMethod = DatasetCodeUtil
				.buildSetParamsContext(getRootJava());
		StatementTarget paramTarget = new StatementTarget(setParamMethod, false);
		for (int i = 0; i < dlg.items.size(); i++) {
			Param[] param = dlg.items.get(i).getParams();
			for (int k = 0; k < param.length; k++) {
				if (param[k] != null) {
					addMethodInvocation(paramTarget, signature, i + "," + k
							+ "," + param[k].toJavaSource(this));
				}
			}
		}
	}

	public Class<?> getProvideClass() {
		return ExecutionUtils.runObject(new RunnableObjectEx<Class<?>>() {
			public Class<?> runObject() throws Exception {
				Object provide = ReflectionUtils.invokeMethod2(getObject(),
						"getProvide");
				return provide == null ? null : provide.getClass();
			}
		});
	}

	public boolean isSysDefine() {
		return ExecutionUtils.runObject(new RunnableObjectEx<Boolean>() {
			public Boolean runObject() throws Exception {
				return (Boolean) ReflectionUtils.invokeMethod2(getObject(),
						"isSysDefine");
			}
		});
	}

	private class BatchPropertyEditor extends TextDialogPropertyEditor {
		@Override
		protected void openDialog(Property property) throws Exception {
			final BatchEditDialog dlg = new BatchEditDialog(Display
					.getDefault().getActiveShell(), SWT.NONE, BatchInfo.this);
			if (dlg.open() == SWT.OK) {
				ExecutionUtils.run(BatchInfo.this, new RunnableEx() {
					public void run() throws Exception {
						editInfo(dlg);
					}
				});
			}
		}

		@Override
		protected String getText(Property property) throws Exception {
			return null;
		}
	}

}

package com.zgw.fireline.design;

import java.security.Permission;

/**
 * RMI 类安全机制管理
 * */
public class RMISecurityManager extends SecurityManager {
	public RMISecurityManager() {
	}

	public void checkPermission() {
	}

	@Override
	public void checkPermission(Permission arg0) {
	}

	@Override
	public void checkPermission(Permission perm, Object context) {
	}
}

package com.zgw.fireline.design.service;

import java.io.File;
import java.net.URL;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 设计器常用服务类
 * */
public interface IDesignCommonService extends Remote {

	/**
	 * 打开组件编辑器
	 * 
	 * @param outPut
	 *            文件输出路径
	 * @param projectName
	 *            工程名称
	 * @param src
	 *            源代码路径
	 * @param classPaths
	 *            工程类路径
	 * @param widgetName
	 *            组件名称
	 * */
	public void openWidgetEdit(File outPut, String projectName, URL src,
			URL[] classPaths, String widgetName) throws RemoteException;

	/**
	 * 清理指定的工程 并重新编译
	 * 
	 * @param projectName
	 *            项目ID
	 * @param outPut
	 *            工程对应的编译输出路径
	 */
	public boolean cleanBuild(String projectName, File outPut)
			throws RemoteException;

	/**
	 * 删除指定工程目录下的资源
	 * */
	public boolean delete(String projectName, String path)
			throws RemoteException;

	/**
	 * 删除指定工程目录下的资源路径
	 * */
	public String getLocation(String projectName, String path)
			throws RemoteException;

}
